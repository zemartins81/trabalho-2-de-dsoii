/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Caio
 */
@Stateless
@LocalBean
public class FuncionarioFachada extends GenericDao<Funcionario> {

    public FuncionarioFachada() {
        super(Funcionario.class);
    }
    
    public List<Funcionario> getListaFuncionarios(){
        return this.findAll();
    }


    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
