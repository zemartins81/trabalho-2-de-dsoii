/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Caio
 */
@Entity
@Table(name = "PRATO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prato.findAll", query = "SELECT p FROM Prato p")
    , @NamedQuery(name = "Prato.findByIdPrato", query = "SELECT p FROM Prato p WHERE p.idPrato = :idPrato")
    , @NamedQuery(name = "Prato.findByNome", query = "SELECT p FROM Prato p WHERE p.nome = :nome")
    , @NamedQuery(name = "Prato.findByTipo", query = "SELECT p FROM Prato p WHERE p.tipo = :tipo")
    , @NamedQuery(name = "Prato.findByValor", query = "SELECT p FROM Prato p WHERE p.valor = :valor")})
public class Prato implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PRATO")
    private Integer idPrato;
    @Size(max = 255)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 255)
    @Column(name = "TIPO")
    private String tipo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private Double valor;

    public Prato() {
    }

    public Prato(Integer idPrato) {
        this.idPrato = idPrato;
    }

    public Integer getIdPrato() {
        return idPrato;
    }

    public void setIdPrato(Integer idPrato) {
        this.idPrato = idPrato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrato != null ? idPrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prato)) {
            return false;
        }
        Prato other = (Prato) object;
        if ((this.idPrato == null && other.idPrato != null) || (this.idPrato != null && !this.idPrato.equals(other.idPrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.Prato[ idPrato=" + idPrato + " ]";
    }
    
}
