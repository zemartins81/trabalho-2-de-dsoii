/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Caio
 */
@Entity
@Table(name = "RELACAO_PRATO_INGREDIENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RelacaoPratoIngrediente.findAll", query = "SELECT r FROM RelacaoPratoIngrediente r")
    , @NamedQuery(name = "RelacaoPratoIngrediente.findByIdRelacao", query = "SELECT r FROM RelacaoPratoIngrediente r WHERE r.idRelacao = :idRelacao")
    , @NamedQuery(name = "RelacaoPratoIngrediente.findByIdPrato", query = "SELECT r FROM RelacaoPratoIngrediente r WHERE r.idPrato = :idPrato")
    , @NamedQuery(name = "RelacaoPratoIngrediente.findByIdIngrediente", query = "SELECT r FROM RelacaoPratoIngrediente r WHERE r.idIngrediente = :idIngrediente")
    , @NamedQuery(name = "RelacaoPratoIngrediente.findByQtde", query = "SELECT r FROM RelacaoPratoIngrediente r WHERE r.qtde = :qtde")})
public class RelacaoPratoIngrediente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_RELACAO")
    private Integer idRelacao;
    @Column(name = "ID_PRATO")
    private Integer idPrato;
    @Column(name = "ID_INGREDIENTE")
    private Integer idIngrediente;
    @Column(name = "QTDE")
    private Integer qtde;

    public RelacaoPratoIngrediente() {
    }

    public RelacaoPratoIngrediente(Integer idRelacao) {
        this.idRelacao = idRelacao;
    }

    public Integer getIdRelacao() {
        return idRelacao;
    }

    public void setIdRelacao(Integer idRelacao) {
        this.idRelacao = idRelacao;
    }

    public Integer getIdPrato() {
        return idPrato;
    }

    public void setIdPrato(Integer idPrato) {
        this.idPrato = idPrato;
    }

    public Integer getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(Integer idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public Integer getQtde() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde = qtde;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRelacao != null ? idRelacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RelacaoPratoIngrediente)) {
            return false;
        }
        RelacaoPratoIngrediente other = (RelacaoPratoIngrediente) object;
        if ((this.idRelacao == null && other.idRelacao != null) || (this.idRelacao != null && !this.idRelacao.equals(other.idRelacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.RelacaoPratoIngrediente[ idRelacao=" + idRelacao + " ]";
    }
    
}
