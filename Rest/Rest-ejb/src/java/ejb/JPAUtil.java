/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jcmar
 */
public class JPAUtil {

    private static JPAUtil instance;
    private final EntityManagerFactory factory;
    

    private JPAUtil() {
        this.factory = Persistence.createEntityManagerFactory("Rest-ejbPU");
    }
    
    /**
     *
     * @return
     */
    public static synchronized JPAUtil getInstance() {
        if (instance != null) {
            return instance;
        }
        return instance = new JPAUtil();
    }

    
    public EntityManager getEntityManger(){
        return factory.createEntityManager();
    }

}
