package ejb;


import java.io.Serializable;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityManager;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jcmar
 * @param <T>
 */
public abstract class GenericDao<T extends Serializable> {

    private Class<T> aClass;

    protected GenericDao(Class<T> aClass) {
        this.aClass = aClass;
    }

    protected EntityManager getEntityManager() {
        return JPAUtil.getInstance().getEntityManger();
    }

    /**
     *
     * @param entity
     */
    public void save(T entity) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        manager.persist(entity);
        manager.getTransaction().commit();
        manager.close();
    }

    public void update(T entity) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        manager.merge(entity);
        manager.getTransaction().commit();
        manager.close();
    }

    public void delete(Integer id) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        manager.remove(manager.find(aClass, id));
        manager.getTransaction().commit();
        manager.close();
    }

    public T findById(Integer id) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        T entity = (T) manager.find(aClass, id);
        manager.getTransaction().commit();
        manager.close();
        return entity;
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        Query query = manager.createQuery("from " + aClass.getSimpleName());
        List<T> entitites = query.getResultList();
        manager.getTransaction().commit();
        manager.close();
        return entitites;
    }

    @SuppressWarnings("unchecked")
    public List<T> find(String jpql, Object... params) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        Query query = manager.createQuery(jpql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i + 1, params[i]);
        }
        List<T> entities = query.getResultList();
        manager.getTransaction().commit();
        manager.close();
        return entities;
    }
    
    @SuppressWarnings("unchecked")
    public T findOne(String jpql, Object... params) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        Query query = manager.createQuery(jpql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i + 1, params[i]);
        }
        T entity = (T) query.getSingleResult();
        manager.getTransaction().commit();
        manager.close();
        return entity;
    }
    
    public long count(){
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        
        Query query = manager.createQuery("select count(c) from " + aClass.getSimpleName() + "c");
        
        long count = (long) query.getSingleResult();
        manager.getTransaction().commit();
        manager.close();
        return count;
    }
    
    
    
}
