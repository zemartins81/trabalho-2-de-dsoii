/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ejb.Funcionario;
import ejb.FuncionarioFachada;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Caio
 */
@Named(value = "funcionarioMBean")
@RequestScoped
public class FuncionarioMBean {

    @EJB
    private FuncionarioFachada funcionarioFachada;

    /**
     * Creates a new instance of FuncionarioMBean
     */
    public FuncionarioMBean() {
    }
    
     public List<Funcionario> getListaFuncionarios() {
        return funcionarioFachada.getListaFuncionarios();
    }
    
}
